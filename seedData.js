const path = require('path');
const { Pool, Client } = require('pg');

const pool = new Pool({
  user: 'naveen',
  host: 'localhost',
  database: 'postgres',
  password: 'naveentiwariji96',
  port: 5432
});

function dropTable() {
  try {
    return pool.query('DROP TABLE IF EXISTS cities,hospitals');
  } catch (err) {
    throw err;
  }
}
function createCitiesTable() {
  try {
    return pool.query(
      'CREATE TABLE cities(hospital_id INT,hospital_name VARCHAR,city VARCHAR,address VARCHAR,rating FLOAT)'
    );
  } catch (err) {
    throw err;
  }
}

function insertDataInCitiesTable() {
  const pathForCities = path.resolve('./', 'sampleData/cities.csv');
  try {
    return pool.query(
      `COPY cities FROM '${pathForCities}' with delimiter ',' csv header`
    );
  } catch (err) {
    throw err;
  }
}
// const pathFOrMatchesTable = path.resolve('../', 'data/matches.csv');
// function insertDataInMatchTable() {
//   return pool.query(
//     `COPY matches FROM '${pathFOrMatchesTable}' with delimiter ',' csv header`
//   );
// }
function creatHospitalsTable() {
  try {
    return pool.query(
      'CREATE TABLE hospitals(id INT,name VARCHAR,hospital_type VARCHAR,doctor1 VARCHAR,doctor2 VARCHAR)'
    );
  } catch (err) {
    throw err;
  }
}
function insertDataInHospitalsTable() {
  const pathForHospitals = path.resolve('./', 'sampleData/hospitals.csv');
  try {
    return pool.query(
      `COPY hospitals from '${pathForHospitals}' with delimiter ',' csv header`
    );
  } catch (err) {
    throw err;
  }
}

function usersTable() {
  try {
    return pool.query(
      'CREATE TABLE IF NOT EXISTS users(user_name VARCHAR,email VARCHAR PRIMARY KEY,password VARCHAR,role VARCHAR)'
    );
  } catch (err) {
    throw err;
  }
}
usersTable();

dropTable()
  .then(() => createCitiesTable())
  .then(() => insertDataInCitiesTable())
  .then(() => creatHospitalsTable())
  .then(() => insertDataInHospitalsTable());
