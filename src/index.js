const cities = require('./routes/cities.js');
const hospitals = require('./routes/hospitals.js');
const users = require('./routes/users.js');
const express = require('express');
require('dotenv').config();
const logger = require('./middleware/logger.js');
const errorHandler = require('./middleware/errorHandler.js');
const app = express();
app.use(express.json());
app.use(logger.log);
app.get(
  '/',
  (request, response) => {
    response.json({ info: 'node.js,Express,and pstgres API' });
  },
  errorHandler
);
app.use('/', users, errorHandler);
app.use('/cities', cities, errorHandler);
app.use('/hospitals', hospitals, errorHandler);
process.on('unhandledRejection', error => {
  console.log('unhandledRejection', error.message);
});
app.use('*', function(req, res) {
  res.status(404).send(`Invalid Url - 404 Page`);
});
const port = process.env.Port || 3000;
app.use(logger.error);
app.listen(port, function(err) {
  if (err) {
    console.log('error while starting server');
  } else {
    console.log(`Listening at port ${port}`);
  }
});
