function errHandler(err, req, res, next) {
  res
    .status(err.statusCode)
    .send(`Error: ${err.message} status: ${err.statusCode}`);
  next(`Error: ${err.message} status: ${err.statusCode}`);
}
module.exports = errHandler;
