const { Pool, Client } = require('pg');

const pool = new Pool({
  user: 'naveen',
  host: 'localhost',
  database: 'postgres',
  password: 'naveentiwariji96',
  port: 5432
});

function getAllHospitals() {
  try {
    return pool
      .query('SELECT * FROM hospitals')
      .then(hospitalsData => hospitalsData.rows);
  } catch (err) {
    throw err;
  }
}
function getHospitalById(id) {
  try {
    return pool
      .query('SELECT * FROM hospitals WHERE id=$1', [id])
      .then(hospital => hospital.rows);
  } catch (err) {
    throw err;
  }
}
function insertValuesInHospitals(insertingValues) {
  try {
    return pool
      .query(
        'INSERT INTO hospitals(id,name,hospital_type,doctor1,doctor2) VALUES ((SELECT MAX(hospital_id) FROM cities)+1,$1,$2,$3,$4)',
        [
          insertingValues.hospital_name,
          insertingValues.hospital_type,
          insertingValues.doctor1,
          insertingValues.doctor2
        ]
      )
      .then(
        pool.query(
          'INSERT INTO cities(hospital_id,hospital_name,city,address,rating) VALUES ((SELECT MAX(hospital_id) FROM cities)+1,$1, $2, $3, $4)',
          [
            insertingValues.hospital_name,
            insertingValues.city,
            insertingValues.address,
            insertingValues.rating
          ]
        )
      );
  } catch (err) {
    throw err;
  }
}
function deleteValuesInHospitals(id) {
  try {
    return pool
      .query(`DELETE FROM hospitals WHERE id=$1`, [id])
      .then(pool.query('DELETE FROM cities WHERE hospital_id=$1', [id]));
  } catch (err) {
    throw err;
  }
}
function updateValuesInHospitals(id, items) {
  try {
    return pool.query(
      `UPDATE hospitals SET name=$2,hospital_type=$3,doctor1=$4,doctor2=$5 WHERE id=$1`,
      [id].concat(Object.values(items))
    );
  } catch (err) {
    throw err;
  }
}

module.exports = {
  getAllHospitals,
  getHospitalById,
  insertValuesInHospitals,
  deleteValuesInHospitals,
  updateValuesInHospitals
};
