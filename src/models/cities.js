const { Pool, Client } = require('pg');

const pool = new Pool({
  user: 'naveen',
  host: 'localhost',
  database: 'postgres',
  password: 'naveentiwariji96',
  port: 5432
});

function getCities() {
  try {
    return pool
      .query('SELECT * FROM cities')
      .then(citiesData => citiesData.rows);
  } catch (err) {
    throw err;
  }
}
function getCityById(id) {
  try {
    return pool
      .query('SELECT * FROM cities WHERE hospital_id=$1', [id])
      .then(city => city.rows);
  } catch (err) {
    throw err;
  }
}

function insertValuesInCities(insertingValues) {
  try {
    return pool
      .query(
        'INSERT INTO cities(hospital_id,hospital_name,city,address,rating) VALUES ((SELECT MAX(hospital_id) FROM cities)+1,$1, $2, $3, $4)',
        [
          insertingValues.hospital_name,
          insertingValues.city,
          insertingValues.address,
          insertingValues.rating
        ]
      )
      .then(
        pool.query(
          'INSERT INTO hospitals(id,name,hospital_type,doctor1,doctor2) VALUES ((SELECT MAX(hospital_id) FROM cities)+1,$1,$2,$3,$4)',
          [
            insertingValues.hospital_name,
            insertingValues.hospital_type,
            insertingValues.doctor1,
            insertingValues.doctor2
          ]
        )
      );
  } catch (err) {
    throw err;
  }
}

function deleteValues(id) {
  try {
    return pool
      .query(`DELETE FROM cities WHERE hospital_id=$1`, [id])
      .then(pool.query('DELETE FROM hospitals WHERE id=$1', [id]));
  } catch (err) {
    throw err;
  }
}

function updateValues(id, items) {
  try {
    return pool.query(
      `UPDATE cities SET hospital_name=$2,city=$3,address=$4,rating=$5 WHERE hospital_id=$1`,
      [id].concat(Object.values(items))
    );
  } catch (err) {
    throw err;
  }
}

module.exports = {
  getCities,
  getCityById,
  insertValuesInCities,
  deleteValues,
  updateValues
};
