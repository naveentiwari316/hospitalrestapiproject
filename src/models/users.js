const { Pool, Client } = require('pg');

const pool = new Pool({
  user: 'naveen',
  host: 'localhost',
  database: 'postgres',
  password: 'naveentiwariji96',
  port: 5432
});

function insertValuesInUsers(name, email, password) {
  try {
    return pool.query(
      "INSERT INTO users(user_name,email,password,role) VALUES ($1, $2, $3, 'user')",
      [name, email, password]
    );
  } catch (err) {
    throw err;
  }
}

function findValidEmail(email) {
  try {
    return pool.query('SELECT * FROM users WHERE email=$1', [email]);
  } catch (err) {
    throw err;
  }
}

module.exports = { insertValuesInUsers, findValidEmail };
