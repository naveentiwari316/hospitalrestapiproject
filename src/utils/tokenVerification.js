const jwt = require('jsonwebtoken');
function authUser(req, res, next) {
  const token = req.header('auth-token');
  if (!token) {
    return res.status(401).send('Access Denied');
  }
  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    req.user = verified;
    console.log(req.user);
    next();
  } catch (err) {
    res.status(400).send('Invalid Token');
  }
}
function authAdmin(req, res, next) {
  const token = req.header('auth-token');
  if (!token) {
    return res.status(401).send('Access Denied');
  }
  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    console.log('hello', verified);
    if (verified.user_name === 'admin') {
      req.user = verified;
      next();
    } else {
      res.send('Access Denied');
    }
  } catch (err) {
    res.status(400).send('Invalid Token');
  }
}
module.exports = { authUser, authAdmin };
