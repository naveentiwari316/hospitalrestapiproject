//REQUIRE JOI
const Joi = require('joi');
const schemaForId = Joi.object({
  //ID SHOULD BE NUMBER,INTEGER,POSITIVE
  id: Joi.number()
    .integer()
    .positive()
});

const schemaForInsert = Joi.object({
  //HOSPITAL NAME SHOULD BE STRING, MINIMUM 3 CHARACTEERS OF LENGTH
  hospital_name: Joi.string()
    .min(3)
    .required(),
  //CITY NAME NAME SHOULD BE STRING, MINIMUM 3 CHARACTEERS OF LENGTH
  city: Joi.string()
    .min(3)
    .required(),
  //HOSPITAL ADDRESS SHOULD BE STRING, MINIMUM 3 CHARACTEERS OF LENGTH
  address: Joi.string()
    .min(3)
    .required(),
  //HOSPITAL NAME SHOULD BE NUMBER
  rating: Joi.number()
    .positive()
    .required(),
  hospital_type: Joi.string()
    .min(3)
    .required(),
  doctor1: Joi.string().min(3),
  doctor2: Joi.string().min(3)
});

const schemaForUpdate = Joi.object({
  hospital_name: Joi.string()
    .min(3)
    .required(),
  city: Joi.string()
    .min(3)
    .required(),
  address: Joi.string()
    .min(3)
    .required(),
  rating: Joi.number()
    .positive()
    .required()
});
const schemaForUpdateHospital = Joi.object({
  name: Joi.string()
    .min(3)
    .required(),
  hospital_type: Joi.string()
    .min(3)
    .required(),
  doctor1: Joi.string()
    .min(3)
    .required(),
  doctor2: Joi.string()
    .min(3)
    .required()
});

//VALIDATION SCHEMA FOR REGISTRATION
const registerValidation = Joi.object({
  //USERNAME SHOULD BE STRING, ALPHA NUMERIC, MINIMUM 3 AND MAXIMUM 30 CHARACTEERS OF LENGTH
  user_name: Joi.string()
    .alphanum()
    .min(3)
    .max(30)
    .required(),
  //EMAIL ID SHOULD BE STRING WITH .COM AND .NET
  email: Joi.string().email({
    minDomainSegments: 2,
    tlds: { allow: ['com', 'net'] }
  }),
  //PASSWORD SHOULD BE MINIMUM 3 AND MAXIMUM 30 CHARACTERS OF LENGTH
  password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/)
});
//VALIDATION SCHEMA FOR LOGIN
const loginValidation = Joi.object({
  //EMAIL ID SHOULD BE STRING WITH .COM AND .NET
  email: Joi.string().email({
    minDomainSegments: 2,
    tlds: { allow: ['com', 'net'] }
  }),
  //PASSWORD SHOULD BE MINIMUM 3 AND MAXIMUM 30 CHARACTERS OF LENGTH
  password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/)
});
module.exports = {
  schemaForId,
  schemaForInsert,
  schemaForUpdate,
  schemaForUpdateHospital,
  registerValidation,
  loginValidation
};
