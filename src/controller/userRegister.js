//REQUIRE USER TABLE
const models = require('../models/users.js');
//REQUIRE VALIDATION SCHEMAS
const validation = require('../utils/validation.js');
//REQUIRE FOR PASSWORD HASHING
const bcrypt = require('bcryptjs');
//REQUIRE FOR WEB TOKEN
const jwt = require('jsonwebtoken');
//REGISTER USER
const registerUser = async function(req, res, next) {
  //CHECKING FOR VALIDATION FOR ENTERED USERNAME, EMAIL AND PASSWORD
  const request = validation.registerValidation.validate(req.body);
  if (request.error) {
    next({ message: request.error.details[0].message, statusCode: 400 });
  } else {
    //HASH PASSWORD
    const salt = await bcrypt.genSalt(7);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);
    //CREATE NEW USER IN DATABASE
    models
      .insertValuesInUsers(req.body.user_name, req.body.email, hashedPassword)
      .then(() => res.status(201).send(`New user sign up success`))
      .catch(err => {
        //IF EMAIL ALREADY EXISTS IN DATABASE
        res.send('Email already exists');
        next({ message: err.message, statusCode: 400 });
      });
  }
};
//LOGIN FOR USER
const loginUser = async function(req, res, next) {
  //CHECKING FOR VALIDATION FOR ENTERED EMAIL AND PASSWORD
  const request = validation.loginValidation.validate(req.body);
  if (request.error) {
    next({ message: request.error.details[0].message, statusCode: 400 });
  } else {
    const validUser = await models.findValidEmail(req.body.email);
    //CHECK FOR VALID EMAIL OR PASSWORD
    console.log('hello1', req.body.email);
    console.log('h2', validUser);
    if (validUser.rowCount == 0) {
      res.status(400);
      next({ message: 'Email incorrect', statusCode: 400 });
    } else {
      //CHECK FOR VALID PASSWORD
      var validPass = await bcrypt.compare(
        req.body.password,
        validUser.rows[0].password
      );
      if (!validPass) {
        res.status(400).send('invalid password');
      } else {
        //CREATING AND ASSIGN A TOKEN
        const token = jwt.sign(
          { user_name: validUser.rows[0].role },
          process.env.TOKEN_SECRET
        );
        res.header('auth-token', token).send('Login Success - ' + token);
      }
    }
  }
};
//EXPORTING FUNCTIONS TO ROUTES
module.exports = {
  registerUser,
  loginUser
};
