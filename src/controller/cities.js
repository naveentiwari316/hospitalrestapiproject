//REQUIRE MODEL FOR CITIES TABLE
const cities = require('../models/cities.js');
//REQUIRE VALIDATION PART
const validation = require('../utils/validation.js');

//GET ALL CITIES HOSPITALS DATA
const getCities = (request, response, next) => {
  cities
    .getCities()
    .then(citiesData => {
      if (citiesData.length != 0) {
        response.send(citiesData);
      } else {
        response.status(404);
        next({
          message: 'No cities data available',
          statusCode: 404
        });
      }
    })
    .catch(err => {
      response.status(400);
      next({ message: 'Bad Request', statusCode: 400 });
    });
};

//GET DETIALS OF CITIES HOSPITAL EACH
const getCityById = (request, response, next) => {
  var getValidation = validation.schemaForId.validate(request.params);
  if (getValidation.error) {
    response.status(400);
    next({ message: getValidation.error.details[0].message, statusCode: 400 });
  } else {
    cities
      .getCityById(request.params.id)
      .then(city => {
        if (city.length != 0) {
          response.status(200).send(city);
        } else {
          response.status(404);
          next({
            message: `city with id ${request.params.id} is not available`,
            statusCode: 404
          });
        }
      })
      .catch(err => {
        response.status(400);
        next({ message: 'Bad Request', statusCode: 400 });
      });
  }
};
const insertValuesInCities = (request, response, next) => {
  var getValidation = validation.schemaForInsert.validate(request.body);
  if (getValidation.error) {
    response.status(400);
    next({ message: getValidation.error.details[0].message, statusCode: 400 });
  } else {
    cities
      .insertValuesInCities(request.body)
      .then(() => {
        response.status(200).send('data inserted successfully');
      })
      .catch(err => {
        response.status(400);
        next({ message: 'Bad Request', statusCode: 400 });
      });
  }
};
const deleteValues = (request, response, next) => {
  var getValidation = validation.schemaForId.validate(request.params);
  if (getValidation.error) {
    response.status(400);
    next({ message: getValidation.error.details[0].message, statusCode: 400 });
  } else {
    cities
      .deleteValues(request.params.id)
      .then(values => {
        if (values.rowCount != 0) {
          response
            .status(200)
            .send(`Delete values of ${request.params.id} id successfully`);
        } else {
          response.status(404);
          next({
            message: `${request.params.id} id is not available`,
            statusCode: 404
          });
        }
      })
      .catch(err => {
        response.status(400);
        next({ message: 'Bad Request', statusCode: 400 });
      });
  }
};
const updateValues = (request, response, next) => {
  var getValidation1 = validation.schemaForId.validate(request.params);
  var getValidation2 = validation.schemaForUpdate.validate(request.body);
  if (getValidation1.error) {
    response.status(400);
    next({ message: getValidation1.error.details[0].message, statusCode: 400 });
  } else if (getValidation2.error) {
    response.status(400);
    next({ message: getValidation2.error.details[0].message, statusCode: 400 });
  } else {
    cities
      .updateValues(request.params.id, request.body)
      .then(data => {
        if (data.rowCount != 0) {
          response
            .status(200)
            .send(`successfully updated ${request.params.id} id values`);
        } else {
          response.status(404);
          next({
            message: `${request.params.id} is not available`,
            statusCode: 404
          });
        }
      })
      .catch(err => {
        response.status(400);
        next({ message: 'Bad Request', statusCode: 400 });
      });
  }
};
module.exports = {
  getCities,
  getCityById,
  insertValuesInCities,
  deleteValues,
  updateValues
};
