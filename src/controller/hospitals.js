const hospitals = require('../models/hospitals.js');
const validation = require('../utils/validation.js');

const getAllHospitals = (request, response, next) => {
  hospitals.getAllHospitals().then(hospitalsData => {
    if (hospitalsData.length != 0) {
      response.send(hospitalsData);
    } else {
      response.status(404);
      next({
        message: 'No hospitals data available',
        statusCode: 404
      });
    }
  });
};

const getHospitalById = (request, response, next) => {
  var getValidation = validation.schemaForId.validate(request.params);
  if (getValidation.error) {
    response.status(400);
    next({ message: getValidation.error.details[0].message, statusCode: 400 });
  } else {
    hospitals
      .getHospitalById(request.params.id)
      .then(hospital => {
        if (hospital.length != 0) {
          response.status(200).send(hospital);
        } else {
          response.status(400);
          next({
            message: `${request.params.id} id is not available`,
            statusCode: 404
          });
        }
      })
      .catch(err => {
        response.status(400);
        next({ message: 'Bad Request', statusCode: 400 });
      });
  }
};
const insertValuesInHospitals = (request, response, next) => {
  var getValidation = validation.schemaForInsert.validate(request.body);
  if (getValidation.error) {
    response.status(400);
    next({ message: getValidation.error.details[0].message, statusCode: 400 });
  } else {
    hospitals
      .insertValuesInHospitals(request.body)
      .then(() => {
        response.status(200).send('data inserted successfully');
      })
      .catch(err => {
        response.status(400);
        next({ message: 'Bad Request', statusCode: 400 });
      });
  }
};
const deleteValuesInHospitals = (request, response, next) => {
  var getValidation = validation.schemaForId.validate(request.params);
  if (getValidation.error) {
    response.status(400);
    next({ message: getValidation.error.details[0].message, statusCode: 400 });
  } else {
    hospitals
      .deleteValuesInHospitals(request.params.id)
      .then(values => {
        if (values.rowCount != 0) {
          response
            .status(200)
            .send(`Delete values of ${request.params.id} id successfully`);
        } else {
          response.status(404);
          next({
            message: `${request.params.id} id is not available`,
            statusCode: 404
          });
        }
      })
      .catch(err => {
        response.status(400);
        next({ message: 'Bad Request', statusCode: 400 });
      });
  }
};
const updateValuesInHospitals = (request, response, next) => {
  var getValidation1 = validation.schemaForId.validate(request.params);
  var getValidation2 = validation.schemaForUpdateHospital.validate(
    request.body
  );
  if (getValidation1.error) {
    response.status(400);
    next({ message: getValidation1.error.details[0].message, statusCode: 400 });
  } else if (getValidation2.error) {
    response.status(400);
    next({ message: getValidation2.error.details[0].message, statusCode: 400 });
  } else {
    hospitals
      .updateValuesInHospitals(request.params.id, request.body)
      .then(data => {
        if (data.rowCount != 0) {
          response
            .status(200)
            .send(`successfully updated ${request.params.id} id values`);
        } else {
          response.status(404);
          next({
            message: `${request.params.id} is not available`,
            statusCode: 404
          });
        }
      })
      .catch(err => {
        response.status(400);
        next({ message: 'Bad Request', statusCode: 400 });
      });
  }
};
module.exports = {
  getAllHospitals,
  getHospitalById,
  insertValuesInHospitals,
  deleteValuesInHospitals,
  updateValuesInHospitals
};
