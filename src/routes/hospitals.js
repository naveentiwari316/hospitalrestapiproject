const express = require('express');
const router = express.Router();
const dataBase = require('../controller/hospitals.js');
const { authUser, authAdmin } = require('../utils/tokenVerification.js');

router.get('/', authUser, dataBase.getAllHospitals);
router.get('/', authAdmin, dataBase.getAllHospitals);
router.get('/:id', authUser, dataBase.getHospitalById);
router.get('/:id', authAdmin, dataBase.getHospitalById);
router.post('/', authAdmin, dataBase.insertValuesInHospitals);
router.delete('/:id', authAdmin, dataBase.deleteValuesInHospitals);
router.put('/:id', authAdmin, dataBase.updateValuesInHospitals);

module.exports = router;
