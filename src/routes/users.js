//REQUIRE EXPRESS
const express = require('express');
const router = express.Router();
const usersController = require('../controller/userRegister.js');
//ROUTES FOR REGISTER USER
router.post('/register', usersController.registerUser);
//ROUTES FOR LOGIN USER
router.post('/login', usersController.loginUser);
//EXPORT THE ROUTE TO INDEX.JS
module.exports = router;
