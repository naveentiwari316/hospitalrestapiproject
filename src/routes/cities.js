const express = require('express');
const router = express.Router();
const dataBase = require('../controller/cities.js');
const { authUser, authAdmin } = require('../utils/tokenVerification.js');

router.get('/', authUser, dataBase.getCities);
router.get('/:id', authUser, dataBase.getCityById);
router.post('/', authAdmin, dataBase.insertValuesInCities);
router.delete('/:id', authAdmin, dataBase.deleteValues);
router.put('/:id', authAdmin, dataBase.updateValues);

module.exports = router;
